from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_script import Manager
from flask_sqlalchemy import SQLAlchemy
import os.path

def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),
            p))

app = Flask(__name__)
app.debug = True

app.config['SECRET_KEY'] = "bcc090e2-26b2-4c16-84ab-e766cc644320"
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../music.db'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['BOOTSTRAP_SERVE_LOCAL'] = True

db = SQLAlchemy(app)


Bootstrap(app)

login_manager = LoginManager(app)

login_manager.login_view = "login"

manager = Manager(app)
