from .app import db, login_manager
from flask_login import UserMixin

ALBUMS_BY_PAGES   = 20
AUTHORS_BY_PAGES  = 20

class Author(db.Model):
    """
    Class Author. An author has for attributes only one name.
    """

    author_id = db.Column(db.Integer, primary_key=True)
    author_name = db.Column(db.String(70))

    def __init__(self, name):
        self.author_name = name

    def __repr__(self):
        return self.author_name

    def get_id(self):
        return self.author_id

    def get_name(self):
        return self.author_name

    def set_name(self, newName):
        self.author_name = newName

        for albums in get_album_by_author(self.author_id):
            albums.set_url()


class Genre(db.Model):
    """
    Class Genre. A Genre has for attributes only one name.
    """

    genre_id = db.Column(db.Integer, primary_key=True)
    genre_name = db.Column(db.String(100))

    def __init__(self, name):
        self.genre_name = name

    def __repr__(self):
        return "<Genre (%d) %s>" % (self.genre_id, self.genre_name)

    def get_id(self):
        return self.genre_id

    def get_name(self):
        return self.genre_name

#Association class between an album and a genre. An album can have multiple genres.
association_genre_album = db.Table("association_genre_album",
                             db.metadata,
                             db.Column("album_id", db.Integer, db.ForeignKey("album.album_id"), primary_key=True),
                             db.Column("genre_id", db.Integer, db.ForeignKey("genre.genre_id"), primary_key=True))

#Association class between an album and a music. An album can have multiple musics.
association_album_music = db.Table("association_album_music",
                          db.metadata,
                          db.Column("album_id", db.Integer, db.ForeignKey("album.album_id"), primary_key = True),
                          db.Column("music_id", db.Integer, db.ForeignKey("music.music_id"), primary_key = True))

class Album(db.Model):
    """
    Album class. An album has for attribute a name,
    an image to show for the users,
    an url used to listen it,
    a year of realisation (annee),
    it is linked with only one author,
    it contains a list of genres and a list of musics.
    """

    album_id = db.Column(db.Integer, primary_key=True)
    album_name = db.Column(db.String(100))
    img = db.Column(db.String(100))
    url = db.Column(db.String(200))
    annee = db.Column(db.String(4))
    composer = db.Column(db.String(100))
    author_id = db.Column(db.Integer, db.ForeignKey("author.author_id"))
    author = db.relationship("Author",
                             backref=db.backref("album", lazy="dynamic"))
    genres = db.relationship("Genre",
                             secondary= association_genre_album, lazy="dynamic",
                             backref = db.backref("genre", lazy=True))
    musics = db.relationship("Music",
                             secondary= association_album_music, lazy="dynamic",
                             backref = db.backref("music", lazy = True))

    def __init__(self, name, img, year, composer, author):
        if name == "" or name == None:
            self.album_name = "untitled"
        else:
            self.album_name = name
        self.annee = year
        if img == None:
            self.img = "default.png"
        else:
            self.img = img

        self.composer = composer
        self.author_id = author

        self.set_url()

    def __repr__(self):
        return "<Album (%d) %s>" % (self.album_id, self.album_name)

    def get_id(self):
        return self.album_id

    def get_name(self):
        return self.album_name

    def get_image(self):
        return self.img

    def get_year(self):
        return self.annee

    def get_author(self):
        return self.author_id

    def get_genres(self):
        return self.genres

    def get_musics(self):
        return self.musics

    def get_url(self):
        return self.url

    def get_composer(self):
        return self.composer

    def set_composer(self, newComposer):
        self.composer = newComposer

    def adding_genre(self, genre):
        """
        Adds a genre to the album.
        """
        self.genres.append(genre)

    def adding_music(self, music):
        """
        Adds a music to the album.
        """
        self.musics.append(music)

    def delete_genre(self, genre):
        """
        Delete a genre to the album.
        """
        self.genres.remove(genre)

    def delete_music(self, music):
        """
        Delete a music to the album and to the database.
        """
        self.musics.remove(music)
        db.session.delete(music)

    def set_name(self, newName):
        self.album_name = newName

        self.set_url()

    def set_year(self, newYear):
        self.annee = newYear

    def set_img(self, newImg):
        self.img = newImg

    def set_author(self, author):
        self.author_id = author

    def set_url(self):
        """
        Update the url of the album and the url of each music in it.
        Used if the name of the album or if the author name has been changed.
        """
        auteur = ""
        elem_author = get_author(self.author_id).get_name().split()
        for i in range(len(elem_author)):
            auteur+=elem_author[i]+"+"

        alb = ""
        elem_name = self.album_name.split()
        for i in range(len(elem_name)):
            alb+=elem_name[i]
            if i != len(elem_name)-1:
                alb+="+"
        self.url = "https://www.youtube.com/results?search_query="+auteur+alb

        for music in self.musics:
            music.set_url(self)

class User(db.Model, UserMixin):
    """
    User class. Only users of the application which have an account and if
    they are connected with can change the information contained in the database.
    A User has a username to be recognized an a password to be connected and protected.
    """

    username = db.Column(db.String(50), primary_key=True)
    password = db.Column(db.String(50))

    def __init__(self, id, passwd):
        self.username = id
        self.password = passwd

    def get_id(self):
        return self.username

    def get_password(self):
        return self.password

    def set_id(self, newId):
        self.username = newId

    def set_password(self, newPass):
        self.password = newPass

class Music(db.Model):
    """
    Music class. A music has a name and an url to listen the music.
    """

    music_id = db.Column(db.Integer, primary_key = True)
    music_name = db.Column(db.String(100))
    url = db.Column(db.String(200))

    def __init__(self, name, alb):
        self.music_name = name

        self.set_url(alb)

    def __repr__(self):
        return "<Music (%d) %s>" % (self.music_id, self.music_name)

    def get_id(self):
        return self.music_id

    def get_name(self):
        return self.music_name

    def get_url(self):
        return self.url

    def set_name(self, newName):
        self.music_name = newName

    def set_url(self, alb):
        """
        Update the url of the music.
        It is used if the album name or if the author name of the
        album has been changed.
        """
        m = ""
        elem_name = self.music_name.split()
        for i in range(len(elem_name)):
            m+=elem_name[i]
            if i != len(elem_name)-1:
                m+="+"
        self.url = alb.get_url()+"+"+m

def get_number_of_pages_album():
    """
    Returns the maximum number of pages which is needed to list all albums.
    Depending to ALBUMS_BY_PAGES constant.
    """
    nblist = Album.query.count()
    if nblist%ALBUMS_BY_PAGES != 0:
        return (nblist//ALBUMS_BY_PAGES)+1
    return nblist//ALBUMS_BY_PAGES

def get_number_of_pages_author():
    """
    Returns the maximum number of pages which is needed to list all authors.
    Depending to AUTHORS_BY_PAGES constant.
    """
    nblist = Author.query.count()
    if nblist%AUTHORS_BY_PAGES != 0:
        return (nblist//AUTHORS_BY_PAGES)+1
    return nblist//AUHTORS_BY_PAGES

def get_authors():
    """
    Returns the list of all authors sorted by name.
    """
    return Author.query.order_by(Author.author_name)

def get_authors_for_page(page):
    """
    Returns the list of authors which will be contained in the page number page
    of the list of authors.
    """
    return get_authors()[(page-1)*AUTHORS_BY_PAGES:page*AUTHORS_BY_PAGES]

def get_author(id):
    """
    Returns the author where the id equals to id.
    """
    return Author.query.get(id)

def get_albums():
    """
    Returns the list of all aalbums sorted by name.
    """
    return Album.query.order_by(Album.album_name)

def get_albums_for_page(page):
    """
    Returns the list of albums which will be contained in the page number page
    of the list of albums.
    """
    return get_albums()[(page-1)*ALBUMS_BY_PAGES:page*ALBUMS_BY_PAGES]

def get_album(id):
    """
    Returns the album where the id equals to id.
    """
    return Album.query.get(id)

def get_album_by_author(id):
    """
    Returns the list of albums made by the author where the id equals to id.
    """
    return Album.query.filter(Album.author_id == id).order_by(Album.annee)

def get_genres():
    """
    Returns a list of all genres which are in the database.
    """
    return Genre.query.all()

def get_genre(id):
    """
    Returns the genre in the database where its id equals to id.
    """
    return Genre.query.get(id)

def get_musics():
    """
    Returns a list of all musics in the database.
    """
    return Music.query.all()

@login_manager.user_loader
def load_user(username):
    """
    Returns the user where its username equals to username.
    """
    return User.query.get(username)

def album_delete(album):
    """
    Delete the album where its id equals to album.
    Delete also all musics in it.
    """
    a = get_album(album)
    musics = a.get_musics()
    for i in musics:
        a.delete_music(i)
    db.session.delete(a)
    db.session.commit()

def author_delete(author):
    """
    Delete the author where its id equals to author.
    Delete also all album he made.
    """
    a = get_author(author)
    albums = get_album_by_author(author)
    for b in albums:
        album_delete(b.get_id())
    db.session.delete(a)
    db.session.commit()
