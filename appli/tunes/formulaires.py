from flask_wtf import FlaskForm
from wtforms import HiddenField, StringField, PasswordField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.validators import DataRequired
from .models import load_user, get_authors



class AlbumForm(FlaskForm):
    """
    Creation and Edition album formular. Used to create or to modify an Album.
    """

    id          = HiddenField("id")
    album_name  = StringField("Nom de votre Album :")
    author      = QuerySelectField("Auteur de votre album :", query_factory = lambda : get_authors())
    listGenres  = StringField("Les genres de votre album (Vous pouvez en mettre plusieurs en les séparant par ';')")
    yearOfReal  = StringField("Année de production")
    composer    = StringField("Compositeur de l'album")
    img         = StringField("Image de votre album :")
    musics      = StringField("Liste des morceaux (Vous pouvez les séparer par ';')")
    next        = HiddenField()

    def __init__(self, id=None, name=None, author=None, listG=None, year=None, composer=None, img=None, listM=None):
        """
        Constructor. Fields of the AlbumForm will be empty for a creation and auto-completed for an
        Edition.
        """
        super().__init__()
        if id:
            self.id.data = id
            self.album_name.data = name
            self.author.data = author
            self.listGenres.data = listG
            self.yearOfReal.data = year
            self.composer.data = composer
            self.img.data = img
            self.musics.data = listM
            self.next.data = "save_album"
        else:
            self.next.data = "new_album_saving"

    def get_id(self):
        return self.id.data

    def get_name(self):
        return self.album_name.data

    def get_author(self):
        return self.author.data

    def get_year(self):
        return self.yearOfReal.data

    def get_image(self):
        return self.img.data

    def get_genres(self):
        return self.listGenres.data.split(";")

    def get_musics(self):
        return self.musics.data.split(";")

    def get_composer(self):
        return self.composer.data

    def set_composer(self, newComposer):
        self.composer.data = newComposer

    def get_next(self):
        return self.next.data

    def set_id(self, newId):
        self.id.data = newId

    def set_name(self, newName):
        self.album_name.data = newName

    def set_author(self, newAuthor):
        self.author.data = newAuthor

    def set_listGenres(self, newList):
        self.listGenres.data = newList

    def set_year(self, newYear):
        self.yearOfReal.data = newYear

    def set_image(self, newImage):
        self.img.data = newImage

    def set_listMusics(self, newList):
        self.musics.data = newList

class AuthorForm(FlaskForm):
    """
    Creation and Edition author formular. Used to create or to modify an Author.
    """

    id   = HiddenField('id')
    name = StringField('Nom', validators = [DataRequired()])
    next = HiddenField()

    def __init__(self, id=None, name=None):
        """
        Constructor. Fields of the AuthorForm will be empty for a creation and auto-completed for an
        Edition.
        """
        super().__init__()
        if id:
            self.id.data = id
            self.name.data = name
            self.next.data = "save_author"
        else:
            self.next.data = "new_author_saving"

    def get_id(self):
        return self.id.data

    def get_name(self):
        return self.name.data

    def get_next(self):
        return self.next.data

    def set_id(self, newId):
        self.id.data = newId

    def set_name(self, newName):
        self.album_name.data = newName

class UserForm(FlaskForm):
    """
    Creation of an User formular.
    """

    username = StringField("Username")
    password = PasswordField("Password")
    confirm  = PasswordField("Confirm Password")

    def get_id(self):
        return self.username.data

    def get_password(self):
        return self.password.data

    def passwordConfirmed(self):
        return self.password.data == self.confirm.data

class LoginForm(FlaskForm):
    """
    Login formular. Used to connect an user to the application.
    """

    username = StringField("Username")
    password = PasswordField("Password")
    next     = HiddenField()

    def get_id(self):
        return self.username.data

    def get_password(self):
        return self.password.data

    def get_authentificated_user(self):
        user = load_user(self.username.data)
        if user is None:
            return None
        from hashlib import sha256
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.get_password() else None

    def get_next(self):
        return self.next.data

    def set_next(self, newNext):
        self.next.data = newNext
