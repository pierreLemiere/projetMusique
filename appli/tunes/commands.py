from .app import manager, db

@manager.command
def loaddb(filename):
    """Creates the tables and populate them with datas in filename.yml."""

    #Creates all tables
    db.create_all()

    #Charging datas
    import yaml
    musics = yaml.load(open(filename, encoding="utf8"))

    #Imports models
    from .models import Author, Album, Genre

    #Charging authors
    authors = {}
    for m in musics:
        a = m["by"]
        if a == None:
            a = "Unknown"
        if a not in authors :
            o = Author(name = a)
            db.session.add(o)
            authors[a] = o
    db.session.commit()

    #Charging types
    genres = {}
    for m in musics:
        for types in m["genre"]:
            if types not in genres:
                o = Genre(name = types)
                db.session.add(o)
                genres[types] = o
    db.session.commit()

    #Charging albums
    tunes = {}
    for m in musics:
        if m["title"] not in tunes:
            if m["by"] == None:
                a = authors["Unknown"]
            else:
                a = authors[m["by"]]
            o = Album(name      = m["title"],
                      img       = m["img"],
                      year      = m["releaseYear"],
                      composer  = m["parent"],
                      author    = a.get_id())
            for types in m["genre"]:
                t = genres[types]
                o.adding_genre(t)
            tunes[m["title"]] = o
            db.session.add(o)
    db.session.commit()

@manager.command
def syncdb():
    """
    Creates all missing tables
    """
    db.create_all()

@manager.command
def newuser(username, password):
    """
    Adds a new user
    """
    from .models import User
    if load_user(username)!=None:
        return "User already exist"
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = User(id  = username,
            passwd = m.hexdigest())
    db.session.add(u)
    db.session.commit()

@manager.command
def passwd(username,newPassword):
    """
    Change the password of username in database
    if he exists in
    """
    from .app import db
    from .models import User
    from hashlib import sha256
    u = User.query.get(username)
    if u!=None:
        m = sha256()
        m.update(newPassword.encode())
        u.set_password(m.hexdigest())
        db.session.commit()
    else:
        return "User doesn't exist. Verify your username and try again."
