from .app import app, db
from flask import render_template, redirect, url_for, request
from flask_login import login_user, logout_user, login_required
from hashlib import sha256
from .models import Author, Genre, Music, Album, User, load_user, get_number_of_pages_album, get_number_of_pages_author, get_author, get_authors, get_authors_for_page, get_album, get_albums, get_albums_for_page, get_album_by_author, get_genres, get_musics, get_genres, get_author, album_delete, author_delete, AUTHORS_BY_PAGES
from .formulaires import AuthorForm, AlbumForm, UserForm, LoginForm


@app.route("/")
def home():
    """
    Redirect the user to the home page.
    """

    return render_template(
        "home.html",
        titre = "Bienvenue à Vous !")

@app.route("/new/album/")
@login_required
def new_album():
    """
    Redirect the user to the page of creation of a new album.
    """
    f = AlbumForm()
    return render_template(
        "new-album.html",
        form = f,
        titre = "Nouvel Album")

@app.route("/new/album/saving/", methods = ("POST",))
def new_album_saving():
    """
    Saves the new album in the database and redirect the user to
    the profile page of the album he created.
    """
    f = AlbumForm()
    if f.validate_on_submit():
        m = Album(name   = f.get_name(),
                  author = f.get_author().get_id(),
                  year   = f.get_year(),
                  composer = f.get_composer(),
                  img    = f.get_image())

        genres = f.get_genres()
        for types in genres:
            o = None
            for t in get_genres():
                if t.get_name() == types:
                    o = t
                    break
            if o == None:
                o = Genre(name = types)
            m.adding_genre(o)

        musics = f.get_musics()
        for title in musics:
            t = Music(name = title, alb = m)
            m.adding_music(t)
        db.session.add(m)
        db.session.commit()
        return redirect(url_for("album", id = m.get_id()))
    return render_template("new-album.html",
                            form = f)

@app.route("/edit/album/<int:id>")
@login_required
def edit_album(id):
    """
    Redirect the user to the page of modification of the album where id equals to id.
    """
    m = get_album(id)
    listG = ""
    for g in m.get_genres():
        listG += g.get_name()+";"

    listM = ""
    for title in m.get_musics():
        listM += title.get_name()+";"

    f = AlbumForm(id     = id,
                  name   = m.get_name(),
                  author = get_author(m.get_author()),
                  listG  = listG,
                  year   = m.get_year(),
                  composer = m.get_composer(),
                  img    = m.get_image(),
                  listM  = listM)

    return render_template(
        "new-album.html",
        form = f,
        titre = m.get_name()+"/edit")

@app.route("/save/album/", methods = ("POST",))
def save_album():
    """
    Saves the modification made to the album in the database and returns the user
    to the profile page of the album.
    """
    f = AlbumForm()
    if f.validate_on_submit():
        a = get_album(f.get_id())

        a.set_name(f.get_name())
        a.set_year(f.get_year())
        a.set_author(f.get_author().get_id())
        a.set_composer(f.get_composer())
        a.set_img(f.get_image())

        listG = f.get_genres()
        for types in a.get_genres():
            if types.get_name() not in listG or types.get_name()=="":
                a.delete_genre(types)
        allG = get_genres()
        for types in listG:
            if types!="":
                o = None
                for t in allG:
                    if t.get_name() == types:
                        o = t
                        break
                if o == None:
                    o = Genre(name = types)
                if o not in a.get_genres():
                    a.adding_genre(o)

        listM = f.get_musics()
        for tune in a.get_musics():
            if tune.get_name() not in listM:
                a.delete_music(tune)
        allM = get_musics()
        for tune in listM:
            if tune != "":
                o = None
                for music in a.get_musics():
                    if music.get_name() == tune:
                        o = music
                        break
                if o == None:
                    o = Music(name = tune, alb = a)
                    a.adding_music(o)
        for music in a.get_musics():
            music.set_url(alb = a)
        db.session.commit()
        return redirect(url_for("album", id = a.get_id()))
    return redirect(url_for("edit_music", id = f.get_id()))

@app.route("/new/author/")
@login_required
def new_author():
    """
    Redirect the user to the page of creation of an author.
    """
    f = AuthorForm()
    return render_template(
        "new-author.html",
        form  = f,
        titre = "Nouvel Autheur")

@app.route("/new/author/saving/", methods=("POST",))
def new_author_saving():
    """
    Saves the new author in the database and redirect the user to his profile page.
    """
    f = AuthorForm()
    if f.validate_on_submit():
        o = Author(name = f.get_name())
        db.session.add(o)
        db.session.commit()
        return redirect(url_for('author', id = o.get_id()))
    return render_template(
        "create-author.html",
        form  = f,
        titre = "Nouvel Autheur")

@app.route("/edit/author/<int:id>")
@login_required
def edit_author(id):
    """
    Redirect the user to the page of modification of the author where its id equals to id.
    """
    a = get_author(id)
    f = AuthorForm(id = a.get_id(),
                   name = a.get_name())
    return render_template(
        "new-author.html",
        author = a,
        form = f,
        titre = a.get_name()+"/edit")

@app.route("/save/author/", methods=("POST",))
def save_author():
    """
    Saves modification made on the author in the database and modify the album if necessary.
    Redirect the User to the profile page of the author.
    """
    a = None
    f = AuthorForm()
    if f.validate_on_submit():
        id = int(f.get_id())
        a  = get_author(id)
        a.set_name(f.get_name())
        db.session.commit()
        return redirect(url_for('author', id = id))
    a = get_author(int(f.get_id()))
    return render_template(
        "edit-author.html",
        author = a,
        form = f)

@app.route("/new/profile/")
def new_profile():
    """
    Redirect the user to Creation page of an account.
    """
    f = UserForm()
    return render_template(
        "new-profile.html",
        form  = f,
        titre = "Nouveau Profil")

@app.route("/new/profile/saving/", methods=("POST",))
def new_profile_saving():
    """
    Saves the new User and redirect it to the home page if the username doesn't already exist.
    Rest in the new profile page if it's the case.
    """
    f = UserForm()
    if f.validate_on_submit() and f.passwordConfirmed() and (load_user(f.get_id()) == None):
        from hashlib import sha256
        m = sha256()
        m.update(f.get_password().encode())
        u = User(id = f.get_id(),
                 passwd = m.hexdigest())
        db.session.add(u)
        db.session.commit()
        return redirect(url_for('home'))
    return render_template(
        "new-profile.html",
        form = f,
        titre = "Nouveau Profil")

@app.route("/login/", methods=("GET","POST",))
def login():
    """
    Redirect the user to the login page and connect it to its account.
    """
    f = LoginForm()
    if not f.is_submitted():
        f.set_next(request.args.get("next"))
    elif f.validate_on_submit():
        user = f.get_authentificated_user()
        if user != None:
            login_user(user)
            next = f.get_next() or url_for("home")
            return redirect(next)
    return render_template(
        "login.html",
        form = f,
        titre = "Connexion")

@app.route("/logout/")
def logout():
    """
    Log out the user and redirect it to the home page.
    """
    logout_user()
    return redirect(url_for("home"))

@app.route("/albums/<int:page>")
def list_albums(page):
    """
    Redirect the user to the page number page of the list of albums.
    """
    listAlbums = get_albums_for_page(page)
    numberOfpages = get_number_of_pages_album()
    return render_template("albums.html",
                           list        = listAlbums,
                           page        = page,
                           pageMin     = max(page-5,1),
                           pageMax     = min(page+10,numberOfpages),
                           page_number = numberOfpages,
                           titre       = "Tous les Albums - "+str(page))

@app.route("/authors/<int:page>")
def list_authors(page):
    """
    Redirect the user to the page number page of the list of authors.
    """
    listAuthors = get_authors_for_page(page)
    numberOfpages = get_number_of_pages_author()
    return render_template("authors.html",
                           list        = listAuthors,
                           page        = page,
                           pageMin     = max(page-5,1),
                           pageMax     = min(page+10,numberOfpages),
                           page_number = numberOfpages,
                           titre       = "Tous les Auteurs - "+str(page))

@app.route("/author/<int:id>")
def author(id):
    """
    Redirect the User to the profile page of the author where its id equals to id.
    """
    a = get_author(id)
    listAlbums = get_album_by_author(id)
    return render_template("author.html",
                            author = a,
                            albums = listAlbums,
                            titre  = a.get_name())

@app.route("/album/<int:id>")
def album(id):
    """
    Redirect the User to the profile page of the album where its id equals to id.
    """
    m = get_album(id)
    return render_template("album.html",
                            album = m,
                            authorName = get_author(m.get_author()),
                            titre = m.get_name())

@app.route("/erase/album/<int:id>")
@login_required
def delete_album(id):
    """
    Delete the album where its id equals to id in the database.
    Redirect the user to the page one of the album list.
    """
    album_delete(id)
    return redirect(
        url_for("list_albums", page=1))

@app.route("/erase/author/<int:id>")
@login_required
def delete_author(id):
    """
    Delete the author where its id equals to id in the database.
    Redirect the user to the page one of the author list.
    """
    author_delete(id)
    return redirect(
        url_for("list_authors", page=1))


@app.route("/recherche/authors/<int:page>", methods = ("GET","POST",))
def search_authors(page):
    mot = request.form["search1"]
    res=[]
    for author in get_authors():
        if mot in author.get_name():
            res.append(author)
    numberOfpages = len(res)//20
    return render_template(
        "authors.html",
        list= res,
        page = page,
        pageMin= 1,
        pageMax = 1,
        page_number = 1,
        titre= "Résultat(s) pour la recherche de "+mot+" "+str(len(res))+" résultat(s)")

@app.route("/recherche/albums/<int:page>", methods = ("GET","POST",))
def search_albums(page):
    mot = request.form["search2"]
    res=[]
    for album in get_albums():
        if mot in album.get_name():
            res.append(album)
    numberOfpages = len(res)//20
    return render_template("albums.html",
        list= res,
        page = page,
        pageMin= 1,
        pageMax = 1,
        page_number = 1,
        titre= "Résultat(s) pour la recherche de "+mot+" "+str(len(res))+" résultat(s)")
